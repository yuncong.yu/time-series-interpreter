# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 10:26:07 2018

@author: dt3t6ux
"""

import os
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.special import comb
from keras.models import load_model
from matplotlib.backends.backend_pdf import PdfPages
from scipy.io import savemat


#%% 
def dtw(array_1, array_2, distance_supplement, weight_distance_supplement):
    """
    dynamic time warping (DTW) for two univariate time series with the same length
    """
    
    # locality constraint
    w = 8
    
    # length of the zwei time series
    n = len(array_1)
        
    DTW = np.full((n+1, n+1), float('inf'))
    DTW[0, 0] = 0
    
    for i in xrange(1, n+1):
        for j in xrange(max(1, i-w), min(n, i+w)+1):
            cost = (1-weight_distance_supplement) * ((i-j)/float(n))**2 + \
                   weight_distance_supplement * (distance_supplement[0][i-1]-distance_supplement[1][j-1])**2
            DTW[i, j] = cost + min(DTW[i-1, j], DTW[i, j-1], DTW[i-1, j-1])
    
    return DTW
    
#%%
def match(array_1, array_2, distance_supplement, weight_distance_supplement):
    """
    find matching points from array_1 to array_2 with the same length and vise versa
    """
    
    n = int(len(array_1))
    
    # matrix of dynamic time warping
    DTW = dtw(array_1, array_2, distance_supplement, weight_distance_supplement)
    
    # initialize matching
    match_1_to_2 = [[] for i_n in xrange(n)]
    match_2_to_1 = [[] for i_n in xrange(n)]
    
    match_1_to_2[n-1].append(n-1)
    match_2_to_1[n-1].append(n-1)
    i = n
    j = n
    
    while (i>1 or j>1):
        
        last_match = np.argmin((DTW[i-1, j], DTW[i, j-1], DTW[i-1, j-1]))
        if last_match == 0:
            match_1_to_2[i-2].append(j-1)
            match_2_to_1[j-1].append(i-2)
            i -= 1
        elif last_match == 1:
            match_1_to_2[i-1].append(j-2)
            match_2_to_1[j-2].append(i-1)
            j -= 1
        elif last_match == 2:
            match_1_to_2[i-2].append(j-2)
            match_2_to_1[j-2].append(i-2)
            i -= 1
            j -= 1
            
    return match_1_to_2, match_2_to_1
    

#%%
def non_max_suppression(array_original, hw=3):
    """
    non-maximum suppression
    input: 
        - array_original: 1D-array to apply non-max suppression
        - hw: half width of non-max suppression filter; int; default 3
    output:
        - array_sup: non-max suppressed array
    """
    
    array_sup = np.copy(array_original)
    n = len(array_original)
    for k in xrange(n):
        if array_original[k] != max( array_original[max(0, k-hw) : min(k+hw+1, n)] ):
            array_sup[k] = 0.
                
    return array_sup


#%%
def assess_half_channel(bs_true, bs_pred):
    """
    assess precision of the detected boundaries in measurement or simulation data in a channel
    input: 
        bs_true: true boundaries in measurement or simulation data in a channel
        be_pred: detected boundaries in measurement or simulation data in a channel
    output:
        cs_right: whether this measurement or simulation data in this channel is perfectly segmented; bool
        bs_right: number of correctly segmented boundaries without multi-detection
        bs_miss: number of not detected boundaries
        bs_multi: number of multi-detected boundaries
        bs_false: number of falsely detected boundaries
    """
    
    # initialize output data
    cs_right = False
    bs_right = 0
    bs_miss = 0
    bs_multi = 0
    bs_false = 0
    
    # number of boundaries
    n_bs_true = len(bs_true)
    n_bs_pred = len(bs_pred)
    
    # initialze error matrix
    bs_error = np.zeros( (n_bs_true, n_bs_pred) )
    
    for i_bs_true in range(n_bs_true):
        
        # number of matching detected boundaries
        n_match = 0
        
        for i_bs_pred in range(n_bs_pred):
            
            bs_error[i_bs_true, i_bs_pred] = abs(bs_true[i_bs_true] - bs_pred[i_bs_pred])
            
            if bs_error[i_bs_true, i_bs_pred] <= 2:
                n_match += 1
                
        if n_match == 0:
            bs_miss += 1
        elif n_match == 1:
            bs_right += 1
        else:
            bs_multi += 1
                
    for i_bs_pred in range(n_bs_pred):
        
        match_exist = False
        
        for i_bs_true in range(n_bs_true):
            
            if bs_error[i_bs_true, i_bs_pred] <= 2:
                match_exist = True
                
        if match_exist == False:
            bs_false += 1
                
    if (bs_miss == 0) and (bs_multi == 0) and (bs_false == 0):
        cs_right = True
    
    return {'cs_right': cs_right, 'bs_right': bs_right, 'bs_miss': bs_miss, 'bs_multi': bs_multi, 'bs_false': bs_false}


#%%
def assess_sample(bs_true, bs_pred):
    """
    assess precision of the detected boundaries in a sample
    input: 
        bs_true: true boundaries of a sample; dictionary with keywords 'measurement' and 'simulation'
        be_pred: detected boundaries of a sample; dictionary with keywords 'measurement' and 'simulation'
    output:
        cs_right: number of perfectly segmented channels
        bs_right: number of correctly segmented boundaries without multi-detection
        bs_miss: number of not detected boundaries
        bs_multi: number of multi-detected boundaries
        bs_false: number of falsely detected boundaries
    """
    
    # initialize output data
    assessment_sample = {
        'cs_right': np.array([0, 0, 0]),
        'bs_right': np.array([0, 0, 0]),
        'bs_miss': np.array([0, 0, 0]),
        'bs_multi': np.array([0, 0, 0]),
        'bs_false': np.array([0, 0, 0]),
    }
    
    # true boundaries in this sample
    bs_mea_true = bs_true['measurement']
    bs_sim_true = bs_true['simulation']
    
    # detected boundaries in this sample
    bs_mea_pred = bs_pred['measurement']
    bs_sim_pred = bs_pred['simulation']
    
    # number of channels
    N = len(bs_mea_true)
    
    # statistics for each channel
    for i in range(N):
        
        # statistics for measurement
        stat_mea = assess_half_channel(bs_mea_true[i], bs_mea_pred[i])
    
        # statistics for simulation
        stat_sim = assess_half_channel(bs_sim_true[i], bs_sim_pred[i])
    
        # register statistics in assessment_sample
        for key in assessment_sample:
            assessment_sample[key] += np.array([stat_mea[key]+stat_sim[key], stat_mea[key], stat_sim[key]])
        if stat_mea['cs_right']==True or stat_sim['cs_right']==True:
            assessment_sample['cs_right'][0] -= 1
            
    return assessment_sample
            

#%%
def round_2_digits(array):
    """
    round every elements in an array to a 2-digit-number
    used for presentation
    """
    return np.array([round(element, 2) for element in array])

#%%
def analyser(ts_mea, ts_sim, threshold=0.4): 
    """
    simultaneously segment one sample consisting of measurement and simulation data
    input:
        ts_mea: measurement data; axis 0 - time (0~99s), axis 1 - N channels;
        ts_sim: simulation data; axis 0 - time (0~99s), axis 1 - N channels;
        threshold: threshold to influence the actual threshold to be regarded as a boundary; float; default 0.4
    output:
        bs_local: detected local boundries; list - N channels, element - np array boundaries in a channel
    """
    
    # length and channel number of the zwei time series
    n, N = map(int, ts_mea.shape)
    
    # initialize bs_local_pred_joint and matching
    bs_local_pred_joint = {'measurement':[], 'simulation':[]}
    match_info = {'mea2sim': [], 'sim2mea': []}
    
    # load cnn
    # for test
#        cnn_path = os.path.join('..', 'cnns', 'analyse', 'to_use', 'model_mixed_1_2018_09_07_085059.h5')
    cnn_path = os.path.join('..', '..', 'cnns', 'analyse', 'to_use', 'model_mixed_1_2018_09_07_085059.h5')
    cnn = load_model(cnn_path)
            
    for i in range(N):
                
        # segment data using cnn
        bs_local_pred_mea_i_raw = cnn.predict(np.array([ts_mea[:, i].reshape(-1, 1)]))[0]
        bs_local_pred_sim_i_raw = cnn.predict(np.array([ts_sim[:, i].reshape(-1, 1)]))[0]
        
        # find matching using dynamic time warping
        distance_supplement = (bs_local_pred_mea_i_raw, bs_local_pred_sim_i_raw)
        match_mea_to_sim, match_sim_to_mea = match(ts_mea[:, i], ts_sim[:, i], distance_supplement, weight_distance_supplement=0.8)
        
        # zip matching infomations for return
        match_info['mea2sim'].append(match_mea_to_sim)
        match_info['sim2mea'].append(match_sim_to_mea)
    
        # initialize bs_pred_joint_mea_i
        bs_local_pred_joint_mea_i = 0.5 * bs_local_pred_mea_i_raw
    
        # jointly segment measurement data
        for i_mea in xrange(n):
            
            n_mea_to_sim = len(match_mea_to_sim[i_mea])
            
            for i_sim in range(n_mea_to_sim):
                
#                weight = comb(len(n_mea_to_sim)-1, i_sim) / float(2**(n_mea_to_sim-1)) * 0.5
                weight = comb(n_mea_to_sim-1, i_sim) / float(2**(n_mea_to_sim))
                bs_local_pred_joint_mea_i[i_sim] += weight * bs_local_pred_sim_i_raw[match_mea_to_sim[i_mea][i_sim]]
        
        # non-max-suppression for bs_local_pred_joint_mea_i
        bs_local_pred_joint_mea_i = non_max_suppression(bs_local_pred_joint_mea_i)
        bs_local_pred_joint_mea_i = np.around(bs_local_pred_joint_mea_i + threshold).astype(int)
        
        # transform one-hot encoding to natural number
        bs_local_pred_joint_mea_i = np.where(bs_local_pred_joint_mea_i) [0][1:-1]
    
        # jointly segment simulation data
        bs_local_pred_joint_sim_i = np.array([], dtype=int)
        for b_mea in bs_local_pred_joint_mea_i:
            bs_local_pred_joint_sim_i = np.append(bs_local_pred_joint_sim_i, match_mea_to_sim[b_mea][0] + np.argmax(bs_local_pred_sim_i_raw[match_mea_to_sim[b_mea]]))

        bs_local_pred_joint['measurement'].append(bs_local_pred_joint_mea_i)
        bs_local_pred_joint['simulation'].append(bs_local_pred_joint_sim_i)
    
    return bs_local_pred_joint, match_info


#%% used to evaluate performance of the analyser
def analyser_evaluate(test_data, n_plot):
    """
    evaluate the performance of analyser
    input:
        test_data: folder name of test data
        n_plot: number of samples to plot
    output:
        pdf file with assessment of the analyser
    """
    
    # load data
    data_path = os.path.join('..', '..', 'data', 'pair_analyse', 'test', test_data, 'samples.pkl')
    with open(data_path, 'r') as f:
        samples = pickle.load(f)

    # report path
    report_path = os.path.join('..', '..', 'reports', 'reports_analyse_analyser', test_data)
    if not os.path.isdir(report_path):
        os.mkdir(report_path)
    plot_path = os.path.join(report_path, 'plots.pdf')
    assessment_path = os.path.join(report_path, 'assessment.csv')
        
    # assessment    
    # initialze assessment
    assessment = {
        'meta': {
            'samples': 0,
            'channels': 0,
            'bs': np.array([0, 0, 0]),
        },
        'precision':{
            # [total, measurement, simulation]
            'cs_right': np.array([0, 0, 0]),
            'cs_right_rate': np.array([0, 0, 0]),
            'bs_right': np.array([0, 0, 0]),
            'bs_right_rate': np.array([0, 0, 0]),
            'bs_miss': np.array([0, 0, 0]),
            'bs_miss_rate': np.array([0, 0, 0]),
            'bs_multi': np.array([0, 0, 0]),
            'bs_multi_rate': np.array([0, 0, 0]),
            'bs_false': np.array([0, 0, 0]),
            'bs_false_rate': np.array([0, 0, 0]),
        }
    }
        
    # number of samples
    assessment['meta']['samples'] = len(samples)
            
    # assessment and plot
    with PdfPages(plot_path) as pp:
        
        # for debug
#        for i_sample in range(3):
        for i_sample in xrange(len(samples)):
    
            # extract data   
            sample = samples[i_sample]
            sample_mea = sample['measurement']
            samplel_sim = sample['simulation']
            
            ts_mea = sample_mea['ts']
            ts_sim = samplel_sim['ts']
            
            bs_mea_true = sample_mea['bs_local']
            bs_sim_true = samplel_sim['bs_local']
            
            # HACK: only for plot in papers
#            for bs_ch in bs_mea_true:
#                bs_ch.pop(0)
#                bs_ch.pop(-1)
#            for bs_ch in bs_sim_true:
#                bs_ch.pop(0)
#                bs_ch.pop(-1)
            
            n, N = map(int, ts_mea.shape)
        
            # analyse
            bs_pred, match_info = analyser(ts_mea, ts_sim)
            
            # detected local boundaries
            bs_mea_pred = bs_pred['measurement']
            bs_sim_pred = bs_pred['simulation']
            
            # match information         
            match_mea_to_sim = match_info['mea2sim']
            match_sim_to_mea = match_info['sim2mea']
            
            # HACK: only for plot in papers
            if i_sample == 13:
                sample_path = os.path.join(report_path, 'sample.mat')
                sample = {
                        'ts_mea': ts_mea,
                        'ts_sim': ts_sim,
                        'bs_mea_pred': bs_mea_pred,
                        'bs_sim_pred': bs_sim_pred,
                        'bs_mea_true': bs_mea_true,
                        'bs_sim_true': bs_sim_true,
                        'match_mea_to_sim': match_mea_to_sim,
                        'match_sim_to_mea': match_sim_to_mea
                        }
                savemat(sample_path, sample)
                return
                   
            # register total number of channels
            assessment['meta']['channels'] += N
            
            # register number of boundaries in measurement data (the same in simulation data)
            for i in range(N):
                assessment['meta']['bs'][1] += len(bs_mea_true[i])
            
            # assess precision of this sample
            assesment_sample = assess_sample({'measurement':bs_mea_true, 'simulation':bs_sim_true}, {'measurement':bs_mea_pred, 'simulation':bs_sim_pred})
            
            # register precision in assessment
            for key in assesment_sample:
                assessment['precision'][key] +=assesment_sample[key]
            
            # plot
            if i_sample in n_plot:
                fig = plt.figure()
            
                t = range(n)
        
                for i in range(N):
                    
                    # axes used to hide xlabels except the last subplot
                    ax = plt.subplot(N, 1, i+1)
                                        
                    # plot i-th channel of the time series
                    plt.plot(t, ts_mea[:,i])
                    plt.plot(t, ts_sim[:,i])           
                    
                    # values of time series at true boundaries in i-th channel
                    b_values_mea_true = ts_mea[bs_mea_true[i], i]
                    b_values_sim_true = ts_sim[bs_sim_true[i], i]
                    
                    # plot true local boundaries in i-th channel
                    plt.scatter(bs_mea_true[i], b_values_mea_true)
                    plt.scatter(bs_sim_true[i], b_values_sim_true)
                    
                    # plot detected local boundaries in i-th channel
                    for b_mea_pred in bs_mea_pred[i]: 
                        plt.axvline(x=b_mea_pred, color='#1f77b4' ,ls = '--')
                    for b_sim_pred in bs_sim_pred[i]:
                        plt.axvline(x=b_sim_pred, color='#ff7f0e' ,ls = ':')
                                    
                    # plot boundaries matching measurement to simulation
                    bs_mea = bs_mea_true[i]
                    
                    for i_b_mea in range(len(bs_mea)):
                        
                        b_mea = bs_mea[i_b_mea]
                        bs_sim = match_mea_to_sim[i][b_mea]
                        b_values_sim = ts_sim[bs_sim, i]
                        
                        for i_b_sim in range(len(bs_sim)):
                            plt.plot((b_mea, bs_sim[i_b_sim]), (b_values_mea_true[i_b_mea], b_values_sim[i_b_sim]), color='k', ls='--')
                            
                    if i < N-1:
                        plt.setp(ax.get_xticklabels(), visible=False)
                    plt.ylabel('Channel '+str(i+1))
                    plt.grid(True)
                    
                fig.legend(('measurement', 'simulation'), loc=9, bbox_to_anchor=(0.5, 1), ncol=2)
                plt.xlabel('time') 
                plt.suptitle('Sample '+str(i_sample), y=1.05)
                plt.show()
                pp.savefig(fig)
                plt.close()
    
    # bs and rates in assessment
    assessment['meta']['bs'][0] = 2 * assessment['meta']['bs'][1]   
    assessment['meta']['bs'][2] = assessment['meta']['bs'][1]   
    assessment['precision']['cs_right_rate'] = round_2_digits( assessment['precision']['cs_right'] / float(assessment['meta']['channels']) )
    assessment['precision']['bs_right_rate'] = round_2_digits( assessment['precision']['bs_right'] / map(float, assessment['meta']['bs']) )
    assessment['precision']['bs_miss_rate'] = round_2_digits( assessment['precision']['bs_miss'] / map(float, assessment['meta']['bs']) )    
    assessment['precision']['bs_multi_rate'] = round_2_digits( assessment['precision']['bs_multi'] / map(float, assessment['meta']['bs']) )    
    assessment['precision']['bs_false_rate'] = round_2_digits( assessment['precision']['bs_false'] / map(float, assessment['meta']['bs']) )    
    
    # generate report
    df_precision = pd.DataFrame(assessment['precision'], index=['Overall', 'Measurement', 'Simulation'], \
                                columns=['cs_right', 'cs_right_rate', 'bs_right', 'bs_right_rate', 'bs_miss', 'bs_miss_rate', 'bs_multi', 'bs_multi_rate', 'bs_false', 'bs_false_rate'])
    df_precision.to_csv(assessment_path)
    
    # write meta in the file
    with open(assessment_path, 'r') as f:
        contents = f.readlines()
        
    contents.insert(0, 'samples,'+str(assessment['meta']['samples'])+'\n')
    contents.insert(1, 'channels,'+str(assessment['meta']['channels'])+'\n')
    contents.insert(2, 'total boundaries,'+str(assessment['meta']['bs'][0])+'\n')
    contents.insert(3, 'boundaries in measurement data,'+str(assessment['meta']['bs'][1])+'\n')
    contents.insert(4, 'boundaries in simulation data,'+str(assessment['meta']['bs'][2])+'\n')

    contents = "".join(contents)

    with open(assessment_path, 'w') as f:
        f.write(contents)
        

#%% main function for testing and evaluation of the analyser
if __name__ == "__main__":

    # config
#    test_data = 'test_mixed_3_2018_10_17_094409'
    test_data = 'test_mixed_1_2018_10_29_160308'
    n_plot = range(100)
    
    # assess the performance of analyser 
    analyser_evaluate(test_data, n_plot)