# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 15:10:27 2018

@author: dt3t6ux
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from keras.models import load_model

from model_inspect import model_inspect


def non_max_sup(y_pred, hw=3):
    """
    non-maximum suppression
    input: 
        - y_pred: array to apply non-maximum suppression
        - hw: half width of non-maximum suppression filter; int; default 3
    output:
        - y_pred_sup: non-maximum suppressed array
    """
    
    y_pred_sup = np.copy(y_pred)
    n_samples, n = y_pred.shape
    for i in range(n_samples):
        for j in range(n):
            if y_pred[i, j] != max( y_pred[i, max(0, j-hw):min(j+hw+1, n)] ):
                y_pred_sup[i, j] = 0.
                
    return y_pred_sup


def model_test(model_name, data_train, data_test, samples=range(100), threshold=0.4):
    """
    plot the segmentation result of a random generated time series
    input:
        - model_name: name of the neural network model; string; 
                      e.g. model_mixed_5_2018_08_21_204508.h5
        - data_train: data for training, for documentation purpose; string;
                      e.g. train_mixed_5_2018_08_20_102756
        - data_test: test data; string;
                      e.g. test_mixed_5_2018_08_20_103700
        - samples: samples to be plot; tuple; default range(100) (samples=-1 means all samples)
        - threshold: threshold to influence the threshold to be regarded as a boundary; float; default 0.4
    """
    
    # paths
    model_path = '../../model/' + model_name   
    test_data_path = '../../data/seg_nn/test/' + data_test
    report_path = '../../reports/' + model_name[:-3] + '.pdf'
    
    # load model
    model = load_model(model_path)
    
    # load x_test and y_test
    x_test = np.load(test_data_path+'/x_test.npy')
    y_test = np.load(test_data_path+'/y_test.npy')
    
    # predict
    y_pred = model.predict(x_test)
    y_pred = non_max_sup(y_pred)
    y_pred = np.around(y_pred + threshold).astype(int)
    
    # samples to be plotted
    if samples == -1: 
        samples = range(len(x_test))
    
    # intialize boundary sets and assessment 
    bs_true = []
    bs_pred = []
    assess = {
        # total number of samples
        'n_s': len(x_test),
        # number of totally correct segmented samples
        'n_s_right': [0, 0.],
        # total number of boundaries (exclude 0 and t_end)
        'n_b': 0,
        # number of correctly detected boundaries (number, rate)
        'n_b_right': [0, 0.],
        # number of undetected boundaries (number, rate)
        'n_b_miss': [0, 0.],
        # number of mult-detected boundaries (number, rate)
        'n_b_multi': [0, 0.],
        # number of falsely detections (number, rate)        
        'n_b_false': [0, 0.],
    }
    
    for i in xrange(len(x_test)):
        bs_true.append(np.where(y_test[i])[0][1:-1])
        bs_pred.append(np.where(y_pred[i])[0][1:-1])
        
        assess['n_b'] += len(bs_true[i])
        
        # x_loc is within each sample
        n_b_right_loc = 0
        n_b_miss_loc = 0
        n_b_multi_loc = 0
        n_b_false_loc = 0
        bs_error = np.zeros((len(bs_true[i]), len(bs_pred[i])))
        for j in range(len(bs_true[i])):
            n_corres = 0
            for k in range(len(bs_pred[i])):
                bs_error[j, k] = abs(bs_true[i][j] - bs_pred[i][k])
                if bs_error[j, k] <= 2:
                    n_corres += 1
            if n_corres == 0:
                n_b_miss_loc += 1
            elif n_corres == 1:
                n_b_right_loc += 1
            else:
                n_b_multi_loc += 1
        for j in range(len(bs_pred[i])):
            corres = False
            for k in range(len(bs_true[i])):
                if bs_error[k, j] <= 2:
                    corres = True
            if corres == False:
                n_b_false_loc += 1
                
        assess['n_b_right'][0] += n_b_right_loc
        assess['n_b_miss'][0] += n_b_miss_loc
        assess['n_b_multi'][0] += n_b_multi_loc
        assess['n_b_false'][0] += n_b_false_loc
        
        if (n_b_right_loc == len(bs_true[i])) and (n_b_false_loc == 0):
            assess['n_s_right'][0] += 1
            
    assess['n_s_right'][1] = assess['n_s_right'][0] / float(assess['n_s'])
    assess['n_b_right'][1] = assess['n_b_right'][0] / float(assess['n_b'])
    assess['n_b_miss'][1] = assess['n_b_miss'][0] / float(assess['n_b'])
    assess['n_b_multi'][1] = assess['n_b_multi'][0] / float(assess['n_b'])
    assess['n_b_false'][1] = assess['n_b_false'][0] / float(assess['n_b'])
    
    # generate report
    with PdfPages(report_path) as pp:
        firstPage = plt.figure(figsize=(11.69,8.27))
        firstPage.clf()
        firstPage.text(0.07, 0.9, 'Report Segmentation', transform=firstPage.transFigure, size=36, ha="left")
        txt = model_inspect(model_name) + \
              '\n\nData:' + \
              '\n  - Training data: ' + data_train + \
              '\n  - Test data: ' + data_test + \
              '\n\nResult:' + \
              '\n  - Threshold: {}'.format(threshold) + \
              '\n  - Number of samples: {}'.format(assess['n_s']) + \
              '\n  - Number of erfectly segmented samples: {} ({:.1f}%)'.format(assess['n_s_right'][0], assess['n_s_right'][1]*100) + \
              '\n  - Number of boundaries: {}'.format(assess['n_b']) + \
              '\n  - Number of correctly detected boundaries (excluding multi-detected ones): {} ({:.1f}%)'.format(assess['n_b_right'][0], assess['n_b_right'][1]*100) + \
              '\n  - Number of undetected boundaries: {} ({:.1f}%)'.format(assess['n_b_miss'][0], assess['n_b_miss'][1]*100) + \
              '\n  - Number of multi-detected boundaries: {} ({:.1f}%)'.format(assess['n_b_multi'][0], assess['n_b_multi'][1]*100) + \
              '\n  - Number of falsely detected boundaries: {} ({:.1f}%)'.format(assess['n_b_false'][0], assess['n_b_false'][1]*100) + \
              '\n\nAnnotation for plots:' + \
              '\n  - Yellow solid line: preset boundaries' + \
              '\n  - Blud dashed line: detected boundaries'
        firstPage.text(0.1, 0.35, txt, transform=firstPage.transFigure, size=11, ha="left")
        pp.savefig()
        plt.close()
                
        for i in samples:
            plt.figure()
            for j in range(x_test.shape[2]):
                plt.plot(x_test[i, :, j])    
            for j in bs_true[i]:
                plt.axvline(x=j, color = 'y')
            for j in bs_pred[i]:
                plt.axvline(x=j, color = 'c', ls = '--')
            plt.xlabel('t in [s]') 
            plt.ylabel('value in [unit]')
            plt.title('value - t Diagram')
#            plt.grid(True)
            pp.savefig()
            plt.close
                        
    return assess
        

def main():
    
    # config
    model_name = 'model_mixed_1_2018_08_20_151813.h5'
    data_train = 'train_mixed_1_2018_08_20_150737'
    data_test = 'test_mixed_1_2018_08_20_151256'
    plot_samples = range(100)
    threshold = 0.4
    
    # test model
    model_test(model_name, data_train, data_test, plot_samples, threshold)
    
    
if __name__ == "__main__":
    main()