# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 14:36:06 2018

@author: dt3t6ux
"""

import os
import datetime
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv1D, Flatten
from keras.callbacks import EarlyStopping, ModelCheckpoint


#%% Data Preparation

# config
data_train = 'train_mixed_1_2018_08_20_150737'

# load training data
train_path = os.path.join('..', '..', '..', 'data', 'single_analyse_cnn', 'train', data_train)
x_path = os.path.join(train_path, 'x_train.npy')
y_path = os.path.join(train_path, 'y_train.npy')
x_train = np.load(x_path)
y_train = np.load(y_path)
#x_train = np.float32(np.load(train_path+'x_train.npy'))
#y_train = np.float32(np.load(train_path+'y_train.npy'))

# path to save model
# type of the model
model_type = data_train[6:9]
if model_type == 'lin':
    model_type += 'ear'
elif model_type == 'ste':
    model_type += 'p'
elif model_type == 'mix':
    model_type += 'ed'
# number of channels
N = x_train[0].shape[-1]
model_path = '../../model/model_' + model_type + '_' + str(N) + datetime.datetime.now().strftime("_%Y_%m_%d_%H%M%S") + '.h5'

for i in xrange(len(x_train)):
    x_train[i] = np.array(x_train[i])


#%% build model

# model architecture
model = Sequential([
        Conv1D(8, 5, activation='relu', input_shape=x_train[0].shape, padding='same'),
#        MaxPooling1D(pool_size=3),
        Conv1D(16, 5, activation='relu', padding='same'),
        Conv1D(32, 7, activation='relu', padding='same'),
        Flatten(),
#        Dense(400, activation='relu'),
        Dense(int(y_train[0].shape[0]), activation='sigmoid')])
model.summary()

# compile model    
model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['mse'])

# train model
monitor = EarlyStopping(monitor='val_loss', min_delta=1e-4, patience=5, verbose=1)
checkpoint = ModelCheckpoint(model_path, save_best_only=True)
model.fit(x_train, y_train, callbacks=[monitor, checkpoint] ,validation_split=0.2, epochs=100, verbose=2) 

# save model
#model.save(model_path)

# focal loss function: doesn't work
#def focal_loss(gamma=2., alpha=.25):
#	def focal_loss_fixed(y_true, y_pred):
#		pt_1 = C.element_select(K.equal(y_true, 1), y_pred, K.ones_like(y_pred))
#        	pt_0 = C.element_select(K.equal(y_true, 0), y_pred, K.zeros_like(y_pred))
#        	return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1))-K.sum((1-alpha) * K.pow( pt_0, gamma) * K.log(1. - pt_0))
#	return focal_loss_fixed