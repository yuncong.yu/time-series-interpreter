# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 09:14:18 2018

@author: dt3t6ux
"""

import os
import datetime
import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler


#%% 
def extract_segment(data_raw, n_norm, category, n_plot):
    """
    extract segments and their types from samples with one channel and one data set (measurement or simulation)
    """
    
    # raw data path
    if category == 'debug':
        data_raw_path = os.path.join('..', '..', '..', 'data', 'single_compare_cnn', 'debug', data_raw, 'samples.pkl')
    else:
        data_raw_path = os.path.join('..', '..', '..', 'data', 'single_compare_cnn', category+'_raw', data_raw, 'samples.pkl')

    # normalized time for a segment
    t_norm = range(n_norm)
    scaler_t = MinMaxScaler(feature_range=(0, n_norm-1))
    
    # load data
    with open(data_raw_path, 'r') as f:
        samples = pickle.load(f)
    
    # number of samples
    n_samples = len(samples)
    
    # initialize data for CNN
    x = []
    y = []
    
    for i_sample in xrange(n_samples):    
    
        # extract relevant data
        sample = samples[i_sample]
        ts = sample['ts']
        bs = np.array(sample['bs_local'][0]).astype(np.int32)
        seg_types = sample['seg_types'][0].reshape(-1, 1)
        n = len(ts)
        m = len(bs)+1
        
        # segment data
        b_lf = np.insert(bs, 0, 0) + 1
        b_rt = np.append(bs, n-1)
        
        for i_seg in range(m):
            
            # values in the segment
            seg = ts[b_lf[i_seg]:b_rt[i_seg]]
        
            # scale time to the uniformed length n_norm
            n_seg = len(seg)
            t_seg = np.array(range(n_seg))
            t_seg_norm = scaler_t.fit_transform(t_seg.reshape(-1, 1)).ravel()
            
            # initialize segment time series
            seg_norm = np.zeros(n_norm)
            seg_norm[0] = seg[0]
            seg_norm[-1] = seg[-1]
            
            # resample segment
            for t_point in t_norm[1:-1]:
                
                for i_t in range(n_seg):
                    
                    if t_seg_norm[i_t] > t_point:
                        
                        i_t_lf = i_t-1
                        t_lf = t_seg_norm[i_t-1]
                        i_t_rt = i_t
                        t_rt = t_seg_norm[i_t]
                        
                        break
                    
                dist_lf = t_point - t_lf
                dist_rt = t_rt - t_point
                
                # apply law of the lever
                seg_norm[t_point] = (seg[i_t_lf] * dist_rt + seg[i_t_rt] * dist_lf) / float(t_rt - t_lf)
                
                # float32
                seg_norm = seg_norm.astype(np.float32)
                
                # tranform to column vector
                seg_norm = seg_norm.reshape(-1, 1)

            # extract segment type                                        
            seg_type = seg_types[i_seg]
            
            # one-hot encoding for segment type
            if seg_type == 0:
                seg_type = np.array([1, 0, 0]).astype(np.float32)
            elif seg_type == 1:
                seg_type = np.array([0, 1, 0]).astype(np.float32)
            elif seg_type == 2:
                seg_type = np.array([0, 0, 1]).astype(np.float32)
            
            # register in output 
            x.append(seg_norm)
            y.append(seg_type)
   
    # transform output data to np.array
    x = np.array(x)
    y = np.array(y)
    
    # path to save the data
    save_path = os.path.join('..','..', '..', 'data', 'single_compare_cnn', category, category+'_' + 'seg' + '_' + datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"))
    os.mkdir(save_path)
    x_path = os.path.join(save_path, 'x_'+category+'.npy')
    y_path = os.path.join(save_path, 'y_'+category+'.npy')
    
    # save data
    np.save(x_path, x)
    np.save(y_path, y)
    
    # load saved data
    x_load = np.load(x_path)
    y_load = np.load(y_path)
    
    # plot n_plot samples
    for i_plot in n_plot:
        
        plt.figure()
        plt.plot(x_load[i_plot])
        plt.xlabel('time')
        plt.ylabel('time')
        plt.title('Sample '+str(i_plot)+' type '+str(y_load[i_plot]))
        plt.grid(True)
        plt.show()
        plt.close()
        
        
    

#%% main program
if __name__ == '__main__':

    # config
    
    # data to extract segments from
    data_raw = 'test_mixed_1_2018_10_20_164256'
    
    # normalized time points
    n_norm = 20
    
    # usuage
    # 'train', 'test' or 'debug'
    category = 'test'
    
    # number of samples to plot after generation of all segments
    # used to get a quick picture of the generated data; default range(3)
    n_plot = range(3)
        
    # extract segment
    extract_segment(data_raw, n_norm, category, n_plot)
        
