# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 09:13:23 2018

@author: dt3t6ux
"""

import os
import numpy as np
import itertools
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from keras.models import load_model
from sklearn.metrics import confusion_matrix
from utilities.model_inspect import model_inspect

#%%
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion Matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True')
    plt.xlabel('Predicted')
    plt.tight_layout()


#%% model test
def model_test(model_name, data_train, data_test, n_plot):
    """
    evaluate the performace of the trained cnn for segment types identification
    """
    
    # generate path to cnn model
    model_path = os.path.join('..', '..', '..', 'cnns', 'compare', model_name)
    
    # load cnn model
    model = load_model(model_path)
    
    # generate path to test data
    data_path = os.path.join('..', '..', '..', 'data', 'single_compare_cnn', 'test', data_test)
    x_path = os.path.join(data_path, 'x_test.npy')
    y_path = os.path.join(data_path, 'y_test.npy')
    
    # load test data
    x_test = np.load(x_path)
    y_test = np.load(y_path)
    
    # simple evaluate
    evaluation = model.evaluate(x_test, y_test)
    
    # meta data
    n_samples = len(y_test)
    n_correct_samples = np.rint(n_samples * evaluation[1]).astype(int)
   
    # generate prediction using trained cnn on test data
    y_pred = model.predict(x_test)
    
    # devectorize y_test and y_pred
    y_test = np.argmax(y_test,axis=1)
    y_pred = np.argmax(y_pred,axis=1)
    
    # rearrange the order to 'horizontal', 'linear' and 'PT1'
    y_test = (y_test + 1) % 3
    y_pred = (y_pred + 1) % 3
    
    # confusion Matrix
    cm = confusion_matrix(y_test, y_pred)
    seg_types = ['horizontal', 'linear', 'PT1']
    
    # path to save report
    report_folder = os.path.join('..', '..', '..', 'reports', 'reports_compare_model_test', model_name[:-3])
    if not os.path.isdir(report_folder):
        os.mkdir(report_folder)
    report_path = os.path.join(report_folder, 'report.pdf')
    cm_path = os.path.join(report_folder, 'confusion_matrix.jpg')
    cm_norm_path = os.path.join(report_folder, 'confusion_matrix_normalized.jpg')

    # generate report
    with PdfPages(report_path) as pp:
        
        # save meta data
        firstPage = plt.figure(figsize=(11.69,8.27))
        firstPage.clf()
        firstPage.text(0.07, 0.9, 'Report Segmentation', transform=firstPage.transFigure, size=36, ha="left")
        txt = model_inspect(model_name, model_path) + \
              '\n\nData:' + \
              '\n  - Training data: ' + data_train + \
              '\n  - Test data: ' + data_test + \
              '\n\nResult:' + \
              '\n  - Number of samples: {}'.format(n_samples) + \
              '\n  - Number of correctly categorized samples: {} ({:.1f}%)'.format(n_correct_samples, evaluation[1]*100)
        firstPage.text(0.1, 0.35, txt, transform=firstPage.transFigure, size=11, ha="left")
        pp.savefig()
        plt.close()
        
        # plot confusion matrix without normalization
        plt.figure()
        plot_confusion_matrix(cm, seg_types, title='Confusion Matrix')
        pp.savefig()
        plt.savefig(cm_path)
        
        # plot confusion matrix with normalization (Normalize the confusion matrix by row, i.e by the number of samples in each class)
        plt.figure()
        plot_confusion_matrix(cm, seg_types, normalize=True, title='Confusion Matrix with Normalization by Row')
        pp.savefig()
        plt.savefig(cm_norm_path)
        plt.close
    

#%% main function
if __name__ == "__main__":
    
    # config
    
    # cnn model to use
    model_name = 'model_seg_2018_10_20_131533.h5'
    
    # used training data (for report)
    data_train = 'train_seg_2018_10_20_131437'
    
    # used test data
    data_test = 'test_seg_2018_10_20_164839'
    
    n_plot = range(100)
    
    # test model
    model_test(model_name, data_train, data_test, n_plot)
    