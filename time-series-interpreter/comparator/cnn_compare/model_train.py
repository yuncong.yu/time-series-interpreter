# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 09:13:14 2018

@author: dt3t6ux
"""

import os
import datetime
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv1D, MaxPooling1D, Flatten
from keras.callbacks import EarlyStopping, ModelCheckpoint


#%%
def model_train(data_train):
    """
    train a cnn to identify types of segments
    """
    
    # path to load data
    data_path = os.path.join('..', '..', '..', 'data', 'single_compare_cnn', 'train', data_train)
    x_path = os.path.join(data_path, 'x_train.npy')
    y_path = os.path.join(data_path, 'y_train.npy')
    
    # load traing data
    x_train = np.load(x_path)
    y_train = np.load(y_path)
    
    # path to save the trained cnn model
    model_name = 'model_seg' + datetime.datetime.now().strftime("_%Y_%m_%d_%H%M%S") + '.h5'
    model_path = os.path.join('..', '..', '..', 'cnns', 'cnn_compare', model_name)
    
    # model architecture
    model = Sequential([
        Conv1D(8, 3, activation='relu', input_shape=x_train[0].shape),
        MaxPooling1D(pool_size=2),
        Conv1D(16, 5, activation='relu'),
        Flatten(),
        Dense(32, activation='relu'),
        Dense(int(y_train[0].shape[0]), activation='softmax')])
    model.summary()

    # compile model    
    model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['acc'])
    
    # train model
    monitor = EarlyStopping(monitor='val_loss', min_delta=1e-4, patience=5, verbose=1)
    checkpoint = ModelCheckpoint(model_path, save_best_only=True)
    model.fit(x_train, y_train, callbacks=[monitor, checkpoint] ,validation_split=0.2, epochs=100, verbose=2) 

    
#%% main program
if __name__ == '__main__':

    # config
    data_train = 'train_seg_2018_10_20_131437'
    
    # train model
    model_train(data_train)