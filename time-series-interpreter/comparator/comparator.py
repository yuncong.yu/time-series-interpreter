# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 09:08:59 2018

@author: dt3t6ux
"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from sklearn.preprocessing import MinMaxScaler
from sklearn import linear_model
from keras.models import load_model


#%%
def normalize_seg_horizontal(seg):
    """
    normalize segment horizontally in time axis to fit the input of CNN
    input:
        seg: values of time series; 1D-array
    output: 
        seg_norm: horizontally normalized segment
    """
    
    # length of the segment
    n_seg = len(seg)
    
    # length for CNN input data
    n_norm = 20
    
    # original time axis (may with wrong length)
    t_seg = np.array(range(n_seg))
    
    # goal time axis for a segment (with suitable length and sample rate for CNN)
    t_norm = range(n_norm)
    
    # normalized time axis (with the right length, but may with wrong sample rate)
    scaler_t = MinMaxScaler(feature_range=(0, n_norm-1))
    t_seg_norm = scaler_t.fit_transform(t_seg.reshape(-1, 1)).ravel()
    
    # initialize segment time series
    seg_norm = np.zeros(n_norm)
    seg_norm[0] = seg[0]
    seg_norm[-1] = seg[-1]
    
    # resample segment
    for t_point in t_norm[1:-1]:
        
        for i_t in range(n_seg):
            
            if t_seg_norm[i_t] > t_point:
                
                i_t_lf = i_t-1
                t_lf = t_seg_norm[i_t-1]
                i_t_rt = i_t
                t_rt = t_seg_norm[i_t]
                
                break
            
        dist_lf = t_point - t_lf
        dist_rt = t_rt - t_point
        
        # apply law of the lever
        seg_norm[t_point] = (seg[i_t_lf] * dist_rt + seg[i_t_rt] * dist_lf) / float(t_rt - t_lf)
        
        # float32
        seg_norm = seg_norm.astype(np.float32)
        
    return seg_norm


#%%
def identify_seg_type(seg):
    """
    identify segment type of a segment data set
    input: 
        seg: values of time series; 1D-array
    output: 
        seg_type: segment type
    """
    
    # normalize segment to the right length for CNN input
    seg_norm = normalize_seg_horizontal(seg)
    seg_norm = seg_norm.reshape(-1, 1)
    
    # find CNN model
    # for test
#    model_folder = os.path.join('..', 'cnns', 'compare', 'to_use')
    model_folder = os.path.join('..', '..', 'cnns', 'compare', 'to_use')
    model_name = os.listdir(model_folder)[0]
    model_path = os.path.join(model_folder, model_name)
    
    # apply CNN model
    cnn = load_model(model_path)
    y_pred = cnn.predict(np.array([seg_norm]))
    y_pred = y_pred[0]
    seg_type = np.argmax(y_pred) 
    
    return seg_type


#%%
def regression(seg_type, seg_mea, seg_sim):
    """
    find parameter of a segment (slope of a line segment, time constant of a PT1 component and intercept of a horizontal line)
    """
    
    regr_mea = linear_model.LinearRegression()
    regr_sim = linear_model.LinearRegression()
        
    # linear
    if seg_type == 0:
        
        t_mea = np.arange(len(seg_mea)).reshape(-1, 1)
        t_sim = np.arange(len(seg_sim)).reshape(-1, 1)
        
        seg_mea = seg_mea.reshape(-1, 1)
        seg_sim = seg_sim.reshape(-1, 1)
        
        regr_mea.fit(t_mea, seg_mea)
        regr_sim.fit(t_sim, seg_sim)
        
        slope_mea = regr_mea.coef_[0, 0]
        slope_sim = regr_sim.coef_[0, 0]
        
        para_mea = slope_mea
        para_sim = slope_sim
    
    # PT1 
    # T * y_p_n + y_n = K * u_n
    # T * y_p_n - K * u_n = - y_n
    # central differencing scheme: y_p_n = (y_(n+1) - y_(n-1)) / 2. 
    elif seg_type == 1:
        
        n_seg_mea = len(seg_mea)
        n_seg_sim = len(seg_sim)
        
        u_n_mea = np.full(n_seg_mea-2, 1)
        u_n_sim = np.full(n_seg_sim-2, 1)   
        
        y_p_mea = seg_mea[2:] - seg_mea[:-2]
        y_p_sim = seg_sim[2:] - seg_sim[:-2]
        y_p_mea /= 2. 
        y_p_sim /= 2. 
        
        y_n_mea = seg_mea[1:-1]
        y_n_sim = seg_sim[1:-1]
        
        X_regr_mea = zip(y_p_mea, -u_n_mea)
        X_regr_sim = zip(y_p_sim, -u_n_sim)
        
        y_regr_mea = (-y_n_mea).reshape(-1, 1)
        y_regr_sim = (-y_n_sim).reshape(-1, 1)
        
        regr_mea.fit(X_regr_mea, y_regr_mea)
        regr_sim.fit(X_regr_sim, y_regr_sim)
        
        T_mea = regr_mea.coef_[0, 0]
        T_sim = regr_sim.coef_[0, 0]
        
        para_mea = T_mea
        para_sim = T_sim
    
    # horizontal    
    elif seg_type == 2:
        
        y_mea = np.mean(seg_mea)
        y_sim = np.mean(seg_sim)
        
        para_mea = y_mea
        para_sim = y_sim
        
    return para_mea, para_sim


#%% 
def comparator_seg(seg_mea, seg_sim):
    """
    compare segment type or further parameter of measurement and simulation data in a segment
    input: 
        seg_mea: a segment of measurement data 
        seg_sim: a segment of simulation data
    output:
        error: 
            segment type: [segment type of measurement data, segment type of simulatin data, segment types match?]
            parameter: [parameter of measurement data, parameter of simulatin data, parameters match?]
            converge: float
    """
    
    # initialize output data
    error = {'seg_type': None,
             'parameter': None,
             'converge': None
    }
        
    if len(seg_mea) > 4 and len(seg_sim) > 4:
    
        # identify segment types of measurement and simulation data 
        seg_type_mea = identify_seg_type(seg_mea)
        seg_type_sim = identify_seg_type(seg_sim)
        
        # check segment type
        error['seg_type'] = {'measurement': seg_type_mea,
                             'simulation': seg_type_sim, 
                             'seg_type_match': True
        }
        if seg_type_mea != seg_type_sim:
            error['seg_type']['seg_type_match'] = False
        else:
            # regression to find the parameter for this segment
            para_mea, para_sim = regression(seg_type_mea, seg_mea, seg_sim)
            
            para_diff = abs((para_mea-para_sim)/ float(max(abs(para_mea), abs(para_sim))))
            
            error['parameter'] = {'measurement': para_mea,
                                  'simulation':para_sim,
                                  'difference': para_diff
            }
        
        # check convergence
        error['error'] = {'left': seg_sim[0] - seg_mea[0],
                          'right': seg_sim[-1] - seg_mea[-1],
                          'convergence': None
        }
        
        error['error']['convergence'] = ( (seg_sim[-2:] - seg_mea[-2:]).sum() ) / float( (seg_sim[:2] - seg_mea[:2]).sum() )
        
    return error
    

#%%
def interpret_channel(delays, errors):
    """
    generate inperpretation texts for a channel
    input:
        delays: delays of simulation data compared to measurement data
        errors: errors for each segment in a channal
    output:
        interpretation: interpretation of errors, delays and so on for each segment in this channel; string
    """
    
    # number of segments 
    m = len(delays) - 1
    
    # maps for decoding segment types and parameter types
    map_seg_types = ['linear', 'PT1', 'horizontal']
    map_para_types = ['slope', 'time constant', 'constant value']
    
    # initialize interpretation
    interp = []
    
    # generate interpretation for each segment
    for i_m in range(m):
        
        # heading of interpretation for this segment
        interp_seg = '\n    Segment {}'.format(str(i_m))
        
        # extract delay and error for this segment
        delay = delays[i_m:i_m+2]
        error = errors[i_m]
                
        if error['seg_type'] == None:
            
               interp_seg += '\n      segment too short, unable to find characteristics and corresponding parameters\n'         
        
        else:
            
            # extract segment types
            seg_type_mea = map_seg_types[error['seg_type']['measurement']]
            seg_type_sim = map_seg_types[error['seg_type']['simulation']]
            
            # extract error and convergence
            err_lf = error['error']['left']
            err_rt = error['error']['right']
            convergence = error['error']['convergence']

            # parameter name for this segment
            # will be used only when segment types match
            para_name = map_para_types[error['seg_type']['measurement']]
            
            # when segment types agree
            if error['seg_type']['seg_type_match']: 
                
                interp_seg += '\n      characteristics matched'
                interp_seg += '\n        segment type of the both: {}\n'.format(seg_type_mea)
                
                para_mea = error['parameter']['measurement']
                para_sim = error['parameter']['simulation']
                para_diff = error['parameter']['difference']
                
                if para_diff < 0.2:
                    
                    interp_seg += '\n      {} matched'.format(para_name)
                
                elif para_diff < 0.8:
                    
                    if para_sim < para_mea:
                        interp_seg += '\n      {} for simulation data small to some extent'.format(para_name)
                    else:
                        interp_seg += '\n      {} for simulation data large to some extent'.format(para_name)
                
                else:
                    
                    if para_sim < para_mea:
                        interp_seg += '\n      {} for simulation data too small'.format(para_name)
                    else:
                        interp_seg += '\n      {} for simulation data too large'.format(para_name)
                        
                interp_seg += '\n        {} for measurement data: {:.2e}'.format(para_name, para_mea)
                interp_seg += '\n        {} for simulation data: {:.2e}\n'.format(para_name, para_sim)
                
            # when segment types disagree
            else:
                
                interp_seg += '\n      characteristics mismatched'
                interp_seg += '\n        segment type of measurement data: {}'.format(seg_type_mea)
                interp_seg += '\n        segment type of simulation data: {}\n'.format(seg_type_sim)
        
            # check convergence
            if abs(convergence) < 0.8:
                
                interp_seg += '\n      error converged'
            
            elif abs(convergence) < 1.2:
                
                if convergence < 0:
                    interp_seg += '\n      the absolute value of error stays approximately the same, but has changed direction'
                else:
                    interp_seg += '\n      error stays approximately the same'
                    
            else:
                
                if convergence < 0:
                    interp_seg += '\n      error diverged and direction altered'
                else:
                    interp_seg += '\n      error diverged in the same direction'
                                
            interp_seg += '\n        error of the left end: {:.2e}'.format(err_lf)
            interp_seg += '\n        error of the right end: {:.2e}\n'.format(err_rt)
        
        interp_seg += '\n      delay:'
        interp_seg += '\n        left end: {}'.format(delay[0])
        interp_seg += '\n        right end: {}\n'.format(delay[1])
           
        interp.append(interp_seg)
            
    return interp
        

#%%
def comparator_channel(channel_mea, channel_sim, bs_mea, bs_sim):
    """
    compare measurement and simulation data in a channel 
    input: 
        channel_mea: one channel of the measruement data; 1D-array
        channel_sim: one channel of the simulation data; 1D-array
        bs_mea: boundaries of the channel in the measruement data; 1D-array
        bs_sim: boundaries of the channel in the simulation data; 1D-array
    output:
        delays: delays of simulation data compared to measurement data 
        errors
        interpretation: interpretation of errors, delays and so on for each segment in this channel; string
    """
    
    # change form
    bs_mea = np.array(bs_mea)
    bs_sim = np.array(bs_sim)
    
    # delays
    delays = np.append(np.insert(bs_sim - bs_mea, 0, 0), 0)
    
    # errors
    
    # length of the time series
    n = len(channel_mea)
    
    # number of segments
    m = len(bs_mea) + 1
    
    # boundaries with start and end time 
    bs_end_mea = np.append(np.insert(bs_mea, 0, 0), n-1)
    bs_end_sim = np.append(np.insert(bs_sim, 0, 0), n-1)
    
    # initialize outputs errors
    errors = []
    
    # analyse each segment
    for i_m in range(m):
        
        # extract segment
        b_lf_mea = bs_end_mea[i_m] + 1
        b_rt_mea = bs_end_mea[i_m+1]
        
        b_lf_sim = bs_end_sim[i_m] + 1
        b_rt_sim = bs_end_sim[i_m+1]
        
        seg_mea = channel_mea[b_lf_mea:b_rt_mea]
        seg_sim = channel_sim[b_lf_sim:b_rt_sim]
        
        error_seg = comparator_seg(seg_mea, seg_sim)
        errors.append(error_seg)
        
    # interpret the result
    interpretation = interpret_channel(delays, errors)
    
    return delays, errors, interpretation


#%%
def comparator(ts_mea, ts_sim, bs_mea, bs_sim):
    """
    compare measurement and simulation data in a sample 
    input: 
        ts_mea: time series of the measruement data; nD-array
        ts_sim: time series of the simulation data; nD-array
        bs_mea: boundaries of each channel in the measruement data; nD-array
        bs_sim: boundaries of each channel in the simulation data; nD-array
    output:
        delays: delays of simulation data compared to measurement data; nD-array
        errors
        interpretation: interpretation of errors, delays and so on for each segment in each channel; [[string]]
    """
    
    # number of channals in this sample
    N = ts_mea.shape[1]
    
    # initialize outputs for a sample
    delays_sample = []
    errors_sample = []
    interp_sample = []
    
    # generate interpretation
    for i in range(N):
        
        channel_mea = ts_mea[:, i]
        channel_sim = ts_sim[:, i]
        
        bs_channel_mea = bs_mea[i]
        bs_channel_sim = bs_sim[i]
        
        delays_channel, errors_channel, interp_channel = comparator_channel(channel_mea, channel_sim, bs_channel_mea, bs_channel_sim)
        
        delays_sample.append(delays_channel)
        errors_sample.append(errors_channel)
        interp_sample.append(interp_channel)
        
    return delays_sample, errors_sample, interp_sample
        

#%%
def comparator_evaluate(test_data):
    """
    evaluate the performance of regression and comparator
    input:
        test_data: folder of test data
    output:
        pdf file with assessment of the comparator
    """
    
    # load data
    data_path = os.path.join('..', '..', 'data', 'pair_compare', 'test', test_data, 'samples.pkl')
    with open(data_path, 'r') as f:
        samples = pickle.load(f)
        
    # directory to save results
    report_directory_path = os.path.join('..', '..', 'reports', 'reports_compare_comparator', test_data)
    if not os.path.isdir(report_directory_path):
        os.mkdir(report_directory_path)
        
    ts_plots_path = os.path.join(report_directory_path, 'time_series_plots.pdf')
    interp_path = os.path.join(report_directory_path, 'interpretation.txt')
    eval_para_path = os.path.join(report_directory_path, 'evaluation_parameter_estimation.pdf')
    
    # number of samples    
    n_samples = len(samples)
    
    # segment types 
    seg_types_text = ['linear', 'pt1', 'horizontal']    
    
    # initialize error statistics for parameter
    err_para = {'linear': [],
                'pt1': [],
                'horizontal': []
    }
    
    # initialize interpretation for all samples
    interp = []

    # open file to save time series plots
    pp = PdfPages(ts_plots_path)
    
    # HACK: for debug
#    for i_sample in range(3):
    for i_sample in xrange(n_samples):
        
        # extract sample
        sample = samples[i_sample]
        sample_mea = sample['measurement']
        sample_sim = sample['simulation']
    
        ts_mea = sample_mea['ts']
        ts_sim = sample_sim['ts']
    
        bs_mea = sample_mea['bs_local']
        bs_sim = sample_sim['bs_local']
        
#        seg_types_mea = sample_mea['seg_types']
#        seg_types_sim = sample_sim['seg_types']
        
        para_mea = sample_mea['parameters']
        para_sim = sample_sim['parameters']
        
        scaler = sample_mea['scaler']
        
        # number of channals in this sample
        N = ts_mea.shape[1]
        
        # initialize interpretation for a sample
        interp_sample = []
                
        # check interpretation
        for i in range(N):
            
            channel_mea = ts_mea[:, i]
            channel_sim = ts_sim[:, i]
            
            bs_channel_mea = bs_mea[i]
            bs_channel_sim = bs_sim[i]
            
            delays, errors, interp_channel = comparator_channel(channel_mea, channel_sim, bs_channel_mea, bs_channel_sim)
            
            # number of segments
            m = len(bs_channel_mea) + 1
            
            # for evaluation of parameter estimation
            for i_m in range(m):
                
                error = errors[i_m]
                para_seg_mea = para_mea[i][i_m]
                para_seg_sim = para_sim[i][i_m]
                
                # segment to short, no parameters calculated, ignore this segment
                if error['seg_type'] == None:
                    continue
                
                # segment types do not match, ignore this segment
                if not error['seg_type']['seg_type_match']:
                    continue
                
                seg_type = error['seg_type']['measurement']
                
                # normalize parameter 
                if seg_type == 0:
                    para_seg_mea_true = para_seg_mea * scaler.scale_[i]
                    para_seg_sim_true = para_seg_sim * scaler.scale_[i]
                elif seg_type == 1:
                    para_seg_mea_true = para_seg_mea
                    para_seg_sim_true = para_seg_sim
                else:
                    para_seg_mea_true = scaler.transform([[para_seg_mea]])[0][0]
                    para_seg_sim_true = scaler.transform([[para_seg_sim]])[0][0]
                    
                para_seg_mea_pred = error['parameter']['measurement']
                para_seg_sim_pred = error['parameter']['simulation']
                
                err_para_seg_mea = (para_seg_mea_pred - para_seg_mea_true) / float(max(abs(para_seg_mea_pred), abs(para_seg_mea_true)))
                err_para_seg_sim = (para_seg_sim_pred - para_seg_sim_true) / float(max(abs(para_seg_sim_pred), abs(para_seg_sim_true)))                    
                
                err_para[seg_types_text[seg_type]].append(err_para_seg_mea)
                err_para[seg_types_text[seg_type]].append(err_para_seg_sim)
                    
            # plot channel            
            plt.figure()
            
            plt.plot(channel_mea)
            plt.plot(channel_sim)
            
            # values of time series at true boundaries in i-th channel
            b_values_mea = channel_mea[bs_channel_mea]
            b_values_sim = channel_sim[bs_channel_sim]
            
            # plot true local boundaries in i-th channel
            plt.scatter(bs_channel_mea, b_values_mea)
            plt.scatter(bs_channel_sim, b_values_sim)
                    
            plt.xlabel('Time')
            plt.ylabel('Values')
            # for multivariate samples 
#            plt.title('Sample {}, Channel {}'.format(str(i_sample), str(i_N)))
            plt.title('Sample {}'.format(str(i_sample)))
            plt.grid(True)
            pp.savefig()
#            plt.show
            plt.close
            
            # save interpretation for this sample
            interp_sample.append(interp_channel)
            
        interp.append(interp_sample)
    
    # close pdf file for time series plots
    pp.close()
            
    # save interpretation
    with open(interp_path, 'w+') as f:
        
        i_sample = 0
        
        for interp_sample in interp:
            
            f.write('Sample {}'.format(i_sample))
            
            for interp_channel in interp_sample:
                
                for interp_seg in interp_channel:
                    
                    f.write(interp_seg)
                    
                f.write('\n')
                
            i_sample += 1
    
    # plot and save evaluation of relative errors of parameter estimation
    with PdfPages(eval_para_path) as pp:
        for para_type in seg_types_text: 
            plt.figure()
            if para_type == 'linear':
                num_bins = 30
                plt.xlim(-0.06, 0.06)
                plt.ylim(0, 50)
                para_type_for_plot = 'Linear'
            elif para_type == 'pt1':
                num_bins = 30
                plt.xlim(-0.8, 0.2)
                plt.ylim(0, 6)
                para_type_for_plot = 'PT1'
            elif para_type == 'horizontal':
                num_bins = 180
                plt.xlim(-0.2, 0.2)
                plt.ylim(0, 50)
                para_type_for_plot = 'Horizontal'
            plt.hist(err_para[para_type], num_bins, density=True, alpha=0.5)
            plt.xlabel('Relativ Errors')
            plt.ylabel('Probability Density')
            plt.title('{}-Segment'.format(para_type_for_plot))
            plt.grid(True)
            pp.savefig()
#            plt.show
            plt.close


#%% main function for testing and evaluation of the analyser
if __name__ == "__main__":

    # config
    test_data = 'test_mixed_1_2018_10_29_160308'
    
    # assess the performance of analyser 
    comparator_evaluate(test_data)
    