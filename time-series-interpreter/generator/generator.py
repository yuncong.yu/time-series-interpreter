# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 09:28:32 2018

@author: dt3t6ux
"""

import os
import datetime
import pickle
import copy
import numpy as np
import matplotlib.pyplot as plt
from random import sample 
from sklearn.preprocessing import MinMaxScaler
from itertools import izip


#%%
def gen_seg(tspan, seg_0, length, seg_type, delta, f_gauss, f_sin):
    """
    generate a single segment
    input:
        - tspan: length of the whole time series
        - seg_0: last value of the last segment
        - length: length of the segment
        - seg_type: type of the segment
        - delta: range of the segment
    output: 
        - seg: values in the segment
        - parameter: slope for linear segment, time constand for pt1 and None for step
    """
    # linear segment
    if seg_type == 0:
        slope = delta / float(length)
        seg = np.linspace(seg_0, seg_0+delta, length+1)[1:]
        parameter = slope
        
    # pt1 segment
    elif seg_type == 1:
        # time constant
        T = np.random.uniform(low=1., high=tspan/float(10))
        seg = np.array([seg_0 + delta*(1-np.exp(-1./T*k)) for k in range(length)])
        parameter = T
                
    # step
    elif seg_type == 2:
        seg = np.full(length, seg_0+delta)
        parameter = seg_0+delta
    
    # gaussian distributed noise
    seg += np.random.normal(0, scale=f_gauss*abs(delta), size=length)
     
    # sinusoidal noises
    n_sin = 5
    omegas = np.random.uniform(low=20*np.pi/tspan, high=np.pi, size=n_sin) # at least 10 waves, at most with Nyquist rate (2 Hz)
    phases = np.random.uniform(high=2*np.pi, size=n_sin)
    for i_sin in range(n_sin):
        seg += np.random.normal(0, f_sin*abs(delta)) * np.sin(omegas[i_sin]*np.array(range(length))+phases[i_sin])
    
    # return
    return seg, parameter


#%%
def gen_ts(tspan, N, bs_local, m_local, seg_types, deltas, f_gauss, f_sin):
    """
    generate a single time series
    """
    # initialize time series
    ts = np.zeros((tspan + 1, N))
    
    #% initialize the parameter of each segment
    parameters = []
    for i in range(N):
          parameters.append(np.zeros(m_local[i], dtype=np.float32)) 
          
    # loop to generate data in i-th channel
    for i in range(N):

        #% loop to generate data in j-th segment of i-th channel
        for j in range(m_local[i]):
            
            # indeces
            b_j_lf = bs_local[i][j]
            b_j_rt = bs_local[i][j+1]
            
            # prepare parameter for gen_seg
            seg_0 = ts[b_j_lf, i]
            length = b_j_rt - b_j_lf
            
            # run gen_seg
            seg, parameters[i][j] = gen_seg(tspan, seg_0, length, seg_types[i][j], deltas[i][j], f_gauss, f_sin)
            
            # integrate seg in ts
            ts[b_j_lf+1:b_j_rt+1, i] = seg
            
            # for j==0 when seg_type=0
            if seg_types[i][0]==2:
                ts[0 ,i] = ts[1, i]

    return ts, parameters

#%% 
def gen_samples_single(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin):
    """
    generate data with single time series per sample
    return: 
        samples with
        - ts: axis values in time series; 0 - sample, axis 1 - time, axis 2 - channel
        - bs_global: array with global boundaries; axis 0 - sample, axis 1 - channel
        - bs_local: array with local boundaries; axis 0 - sample, axis 1 - channel
        - seg_types: array with segment types; axis 0 - sample, axis 1 - channel
        - parameters: array with time constant / slope / step; axis 0 - sample, axis 1 - channel
        - scaler
    """
    # initialze output data
    samples = []
    
    #% loop to generate each sample
    for i_sample in xrange(n_samples):
                    
        #% global boundaries
        
        # randomly initialize number of global boundaries m_global
        m_global = int(np.random.uniform(m_range[0], m_range[1]))
        
        # randomly pick boundaries, length of segment at least l_min
        bs_global = np.zeros(m_global+1, dtype=int)
        sample_pool = np.arange(l_min, tspan-l_min+1)
        for i_m in range(1, m_global):
            bs_global[i_m] = sample(sample_pool, 1)[0]
            sample_pool = np.setdiff1d(sample_pool, np.arange(bs_global[i_m]-l_min+1, bs_global[i_m]+l_min))
        bs_global[-1] = tspan
        bs_global.sort()

        #% local boundaries
        
        # initialize local boundaries
        bs_local = []
        
        # dropout
        bs_local_drop = [bs_global.copy() for i in range(N)]
        # loop for do dropout for each global boundary
        for i_m in range(1, m_global):
            
            # choose one channel to be forced to implement the boundary
            channel_forced = np.random.randint(N)
            # mark channels to dropout for this boundary i_m as -1
            for i in range(N):
                if i == channel_forced:
                    continue
                else:
                    if np.random.random() < dropout:
                        bs_local_drop[i][i_m] = -1
        for i in range(N):
            bs_local_drop[i] = bs_local_drop[i][bs_local_drop[i] != -1]
            
        # dispersion 
        for i in range(N):
            
            bs_local.append(bs_local_drop[i] + np.random.normal(0, sigma_bs, size=len(bs_local_drop[i])))
            scaler_t = MinMaxScaler(feature_range=(0, tspan))
            bs_local[i] = sorted(np.rint(scaler_t.fit_transform(bs_local[i].reshape(-1, 1)).ravel()).astype(int))

        #% number of boundaries in each channel
                
        # initialize segment number of each channel
        m_local = np.zeros(N)
        
        # loop for bounary number in each channel
        for i in range(N):
            m_local[i] = len(bs_local[i]) - 1
            
        m_local = map(int, m_local)
                        
        #% segment type of each segment
        
        # initialize segment types
        seg_types = []
        
        # loop for segment types in each channel
        for i in range(N):
            seg_types.append(np.random.randint(3, size=m_local[i]))
            
        #% delta of each segment
        
        # initialize the delta of each channel
        deltas = []

        for i in range(N):
            deltas.append(np.random.uniform(low=-1., high=1., size=m_local[i]))
        
        # generate i_sample-th sample 
        ts, parameters = gen_ts(tspan, N, bs_local, m_local, seg_types, deltas, f_gauss, f_sin)
                  
        # normalize values in the time series to [0, 1]
        scaler = MinMaxScaler()
        ts = scaler.fit_transform(ts)  
        
        # delete start and end time point as boundaries
        bs_global = bs_global[1:-1]
        bs_local = [bs_local[i][1:-1] for i in range(len(bs_local))]

        samples.append({'ts':ts.astype(np.float32), 'bs_global':bs_global, 'bs_local':bs_local, 'seg_types':seg_types, 'deltas':deltas,'parameters':parameters, 'scaler':scaler})
    
    return samples


#%% 
def gen_samples_pair(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, mutate_bs_local, mutate_seg_types, mutate_deltas):
    """
    generate data with paired time series per sample
    return: 
        samples with      
        - ts: axis values in time series; 0 - sample, axis 1 - time, axis 2 - channel
        - bs_global: array with global boundaries; axis 0 - sample, axis 1 - channel
        - bs_local: array with local boundaries; axis 0 - sample, axis 1 - channel
        - seg_types: array with segment types; axis 0 - sample, axis 1 - channel
        - parameters: array with time constant / slope / step; axis 0 - sample, axis 1 - channel
        - scaler
    """
    # generate measurement data
    samples_measurement = gen_samples_single(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin)
    
    # initialze simulation data
    samples_simulation = []
        
    # loop to generate simulation data
    for i_sample in xrange(n_samples):
        
        # extract bs_global
        bs_global = samples_measurement[i_sample]['bs_global']
        
        # mutate bs_local
        bs_local = copy.deepcopy(samples_measurement[i_sample]['bs_local'])
        for i in range(len(bs_local)):
            bs_local[i].insert(0, 0)
            bs_local[i].append(tspan)
        bs_local = [bs_i + np.random.normal(0, mutate_bs_local, size = len(bs_i)) for bs_i in bs_local]
        scaler_t = MinMaxScaler(feature_range=(0, tspan))
        bs_local = [sorted(np.rint(scaler_t.fit_transform(bs_local_i.reshape(-1, 1)).ravel()).astype(int)) for bs_local_i in bs_local]
        m_local = np.array(map(len, bs_local)) - 1
        
        # mutate seg_types
        seg_types = samples_measurement[i_sample]['seg_types']
        for i in range(N):
            m_i = len(seg_types[i])
            for j in range(m_i):
                if np.random.random() < mutate_seg_types:
                    seg_types[i][j] = np.random.randint(3)

        # mutate deltas
        deltas = samples_measurement[i_sample]['deltas']
        for i in range(N):
            m_i = len(deltas[i])
            for j in range(m_i):
                deltas[i][j] += np.random.uniform(low=-mutate_deltas*deltas[i][j], high=mutate_deltas*deltas[i][j])
        
        # generate i_sample-th sample for simulation
        ts, parameters = gen_ts(tspan, N, bs_local, m_local, seg_types, deltas, f_gauss, f_sin)
                
        # normalize values to [0, 1]
        scaler = samples_measurement[i_sample]['scaler']
        ts = scaler.transform(ts)
        
        # delete start and end time point as boundaries
        bs_local = [bs_local[i][1:-1] for i in range(len(bs_local))]
        
        # assenble data for simulation in a variable samples_simulation
        samples_simulation.append({'ts':ts.astype(np.float32), 'bs_global':bs_global, 'bs_local':bs_local, 'seg_types':seg_types, 'deltas':deltas,'parameters':parameters, 'scaler':scaler})
        
    # integrate simulation in samples
    samples = [{'measurement':sample_measurement_i, 'simulation':sample_simulation_i} for sample_measurement_i, sample_simulation_i in izip(samples_measurement, samples_simulation)]
    
    return samples

#%%     
def generator_single(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, usuage, category, n_plot):
    """
    generate and save data with single time series per sample
    output: 
        - samples data.pickle 
    """
    
    # generate data
    samples = gen_samples_single(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin)
    
    # save data
    # for test
#    save_folder = os.path.join('..', 'data', 'single_'+usuage, category, category+'_' + 'mixed' + '_' + str(N) + '_' + datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"))
    save_folder = os.path.join('..', '..', 'data', 'single_'+usuage, category, category+'_' + 'mixed' + '_' + str(N) + '_' + datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"))
    os.mkdir(save_folder)
    save_file = os.path.join(save_folder, 'samples.pkl')
    with open(save_file, 'w') as f:
        pickle.dump(samples, f)
        
    # load data
    with open(save_file, 'r') as f:
        samples = pickle.load(f)
    
    # plot n_plot samples
    for i_plot in n_plot:
        t = np.arange(tspan+1)

        sample_i = samples[i_plot]
        ts = sample_i['ts']
        bs_global = sample_i['bs_global']
        bs_local = sample_i['bs_local']
        
        fig = plt.figure()

        for i in range(N):
            
            # plot i-th channel of the time series
            plt.plot(t, ts[:,i], label='Channel '+str(i+1))
            
            # plot local boundaries in i-th channel
            b_values_i = ts[bs_local[i], i]
            plt.scatter(bs_local[i], b_values_i)
        
        # plot global boundaries
        for b_global in bs_global:
            plt.axvline(x=b_global, color = 'y', ls = '--')
        
        fig.legend(loc=9, bbox_to_anchor=(0.5, 1), ncol=N)
        plt.xlabel('Time') 
        plt.ylabel('Values')
        plt.title('Sample '+str(i_plot), pad=40)
        plt.grid(True)
        plt.show()
        plt.close()

  
#%% generate and save data for comparator    
def generator_pair(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, mutate_bs_local, mutate_seg_types, mutate_deltas, usuage, category, n_plot):
    """
    generate and save data for comparator
    output: 
        - data.pickle: samples
    """
    
    # generate data
    samples = gen_samples_pair(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, mutate_bs_local, mutate_seg_types, mutate_deltas)
    
    # save data
    # for test
#    save_folder = os.path.join('..', 'data', 'pair_'+usuage, category, category+'_' + 'mixed' + '_' + str(N) + '_' + datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"))
    save_folder = os.path.join('..', '..', 'data', 'pair_'+usuage, category, category+'_' + 'mixed' + '_' + str(N) + '_' + datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"))
    os.mkdir(save_folder)
    save_file = os.path.join(save_folder, 'samples.pkl')
    with open(save_file, 'w') as f:
        pickle.dump(samples, f)
        
    # load data
    with open(save_file, 'r') as f:
        samples = pickle.load(f)
        
    # plot n_plot samples
    t = np.arange(tspan+1)
    for i_plot in n_plot:
        
        fig = plt.figure()

        sample_i = samples[i_plot]
        sample_measurement_i = sample_i['measurement']
        sample_simulation_i = sample_i['simulation']
        
        bs_global = sample_measurement_i['bs_global']

        ts_measurement = sample_measurement_i['ts']
        bs_local_measurement = sample_measurement_i['bs_local']
        
        ts_simulation = sample_simulation_i['ts']
        bs_local_simulation = sample_simulation_i['bs_local']
        
        for i in range(N):
            
            ax = plt.subplot(N, 1, i+1)
            
            # plot i-th channel of the time series
            plt.plot(t, ts_measurement[:,i])
            plt.plot(t, ts_simulation[:,i])
            
            # plot global boundaries
#            for b_global in bs_global:
#                plt.axvline(x=b_global, color = 'y', ls = '--')

            # values of time series at boundaries in i-th channel
            b_values_measurement_i = ts_measurement[bs_local_measurement[i], i]
            b_values_simulation_i = ts_simulation[bs_local_simulation[i], i]
            
            # plot local boundaries in i-th channel
            plt.scatter(bs_local_measurement[i], b_values_measurement_i)
            plt.scatter(bs_local_simulation[i], b_values_simulation_i)
            
            if i < N-1:
                plt.setp(ax.get_xticklabels(), visible=False)
            plt.ylabel('Channel '+str(i+1))
            plt.grid(True)
        
        fig.legend(('measurement', 'simulation'), loc=9, bbox_to_anchor=(0.5, 1), ncol=2)
        plt.xlabel('time') 
        plt.suptitle('Sample '+str(i_plot), y=1.05)
        plt.show()
        plt.close()
        
        
#%% main program
if __name__ == '__main__':
    
    #% config (only modify this part for generator)
    
        # number of time series to generate
        n_samples = int(1e1)
        
        # duration of time
        # t_begin = 0, t_end = tspan; default 99 (0 ~ 99s)
        tspan = 99
        
        # number of channels
        N = 3
        
        # range of global segment number m_global
        # HACK: large l_min and m_range at the same time may cause conflict
        # default (3, 10) for single_analyse_cnn, (3, 6) for single_compare_cnn, (3, 5) for other pair
        m_range=(3, 6)
        
        # minimum length of a segment
        # HACK: large l_min and m_range at the same time may cause conflict
        # default 5 for single_analyse_cnn, 10 for pair
        l_min = 10
 
        # rate of dropout 
        # neglection of boundaries, not dropout for NN training; default 0.2
        dropout = 0.2
         
        # standard deviation of boundary dispersion
        # default 0.5
        sigma_bs = 0.5
        
        # factor for the amplitude of sinusoidal noises 
        # default 0.01
        f_sin = 0.01
        
        # factor for the amplitude of gaussian distributed noises 
        # default 0.01
        f_gauss = 0.01
        
        # factor for displacements of local boundaries
        # default 2
        mutate_bs_local = 2
        
        # probability for mutation of segment types
        # default 0.1 (10%)
        mutate_seg_types = 0.1 
        
        # factor for range change of segments 
        # default 0.2 (20%)
        mutate_deltas = 0.2 
        
        # mode of generator
        # 'single': one time series per sample for segmentation; 
        # 'pair': two similar time series per sample for comparason.
        mode = 'single'
        
        # usuage of data
        # used to generate path to save the generated data
        # 'analyse_cnn': data used to train the CNN for segmentation; mode must be 'single', default n_samples = 1e6
        # 'analyse': data used to test the performance of the time series analyser; mode must be 'pair'
        # 'compare_cnn': data used to train the CNN for identification of segment types; mode must be 'single', default n_sample =   
        # 'compare': data used to test the performance of time series comparator and interpreter
        usuage = 'analyse_cnn'
        
        # data category
        # 'train_raw', 'train', 'valid', 'test_raw', 'test', 'debug'; used in the directory and file name if needed
        category = 'debug'
        
        # number of samples to plot after generation of all samples
        # used to get a quick picture of the generated data; default range(3)
        n_plot = range(10)
    
    #% generation process
    
        if mode == 'single':
            generator_single(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, usuage, category, n_plot)
        elif mode == 'pair':
            generator_pair(n_samples, tspan, N, m_range, l_min, dropout, sigma_bs, f_gauss, f_sin, mutate_bs_local, mutate_seg_types, mutate_deltas, usuage, category, n_plot)
        else:
            raise ValueError("mode unknown, it can only be 'analyse' or 'compare'")