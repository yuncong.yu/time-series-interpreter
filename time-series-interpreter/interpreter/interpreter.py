# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 09:20:26 2018

@author: dt3t6ux
"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from ts_interpreter.analyser.analyser import analyser
from ts_interpreter.comparator.comparator import interpret_channel, comparator


#%%
def ts_plot(pp, i_sample, ts_mea, ts_sim, bs_mea_true, bs_sim_true, bs_mea_pred, bs_sim_pred, match_mea_to_sim):
    """
    plot and save time series of a sample
    input:
        pp: file path to save the plot
        i_sample: No. of the sample
        ts_mea: time series values of the measurement data
        ts_sim: time series values of the simulation data
        bs_mea_true: true local time series boundaries of the measurement data
        bs_sim_true: true local time series boundaries of the simulation data
        bs_mea_pred: predicted local time series boundaries of the measurement data
        bs_sim_pred: predicted local time series boundaries of the simulation data
    """

    n, N = map(int, ts_mea.shape)

    fig = plt.figure()            

    for i in range(N):
        
        # axes used to hide xlabels except the last subplot
        ax = plt.subplot(N, 1, i+1)
                            
        # plot i-th channel of the time series
        plt.plot(ts_mea[:,i])
        plt.plot(ts_sim[:,i])           
        
        # values of time series at true boundaries in i-th channel
        b_values_mea_true = ts_mea[bs_mea_true[i], i]
        b_values_sim_true = ts_sim[bs_sim_true[i], i]
        
        # plot true local boundaries in i-th channel
        plt.scatter(bs_mea_true[i], b_values_mea_true)
        plt.scatter(bs_sim_true[i], b_values_sim_true)
        
        # plot detected local boundaries in i-th channel
        for b_mea_pred in bs_mea_pred[i]: 
            plt.axvline(x=b_mea_pred, color='#1f77b4' ,ls = '--')
        for b_sim_pred in bs_sim_pred[i]:
            plt.axvline(x=b_sim_pred, color='#ff7f0e' ,ls = ':')
                        
        # plot boundaries matching measurement to simulation
        bs_mea = bs_mea_true[i]
        
        for i_b_mea in range(len(bs_mea)):
            
            b_mea = bs_mea[i_b_mea]
            bs_sim = match_mea_to_sim[i][b_mea]
            b_values_sim = ts_sim[bs_sim, i]
            
            for i_b_sim in range(len(bs_sim)):
                plt.plot((b_mea, bs_sim[i_b_sim]), (b_values_mea_true[i_b_mea], b_values_sim[i_b_sim]), color='k', ls='--')
        
#        if i == 0:
#            plt.title('Sample '+str(i_sample), pad=40)
        
        if i < N-1:
            plt.setp(ax.get_xticklabels(), visible=False)
        plt.ylabel('Channel '+str(i))
        plt.grid(True)
        
    fig.legend(('measurement', 'simulation'), loc=9, bbox_to_anchor=(0.5, 1), ncol=2)
    plt.xlabel('time') 
    plt.suptitle('Sample '+str(i_sample), y=1.05)
    plt.show()
    pp.savefig(fig)
    plt.close()
    
    pp.close()


#%%
def normalize_para(seg_type, para_orig, scaler, i):
    """
    normalize parameter of a segment
    input:
        seg_type: segment type; int
        para_orig: original parameter of the segment; float
        scaler: scaler used to normalize the time series
        i: channel, in which the segment is located
    output: 
        para_norm: normalized parameter of the segment; float
    """
    if seg_type == 0:
        para_norm = para_orig * scaler.scale_[i]
    elif seg_type == 1:
        para_norm = para_orig
    else:
        para_norm = scaler.transform([[para_orig]])[0][0]
        
    return para_norm


#%%
def interpret_true(samples):
    """
    generate true interpretations for verification
    input: 
        samples
    output:
        interp
    """
    
    # initialize interpretation for all samples
    interp = []
    
    for i_sample in xrange(len(samples)):

        sample = samples[i_sample]
        sample_mea = sample['measurement']
        sample_sim = sample['simulation']
        
        ts_mea = sample_mea['ts']
        ts_sim = sample_sim['ts']
        
        bs_mea_true = sample_mea['bs_local']
        bs_sim_true = sample_sim['bs_local']
        
        seg_types_mea = sample_mea['seg_types']
        seg_types_sim = sample_sim['seg_types']
        
        para_mea = sample_mea['parameters']
        para_sim = sample_sim['parameters']
        
        scaler = sample_mea['scaler']

        n, N = map(int, ts_mea.shape)
        
        interp_sample = []
                
        for i in range(N):
            
            # extract time series values of the channel
            channel_mea = ts_mea[:, i]
            channel_sim = ts_sim[:, i]
            
            # extract boundaries for the channel
            bs_channel_mea_true = bs_mea_true[i]
            bs_channel_sim_true = bs_sim_true[i]
            
            # extract segment types of the channel
            seg_types_channel_mea = seg_types_mea[i]
            seg_types_channel_sim = seg_types_sim[i]
            
            # extract parameters of the channel
            para_channel_mea = para_mea[i]
            para_channel_sim = para_sim[i]
            
            # number of segments
            m = len(bs_channel_mea_true) + 1
            
            # boundaries with start and end time 
            bs_end_mea = np.append(np.insert(bs_channel_mea_true, 0, 0), n-1)
            bs_end_sim = np.append(np.insert(bs_channel_sim_true, 0, 0), n-1)
            
            # construct delays and errors
            delays = bs_end_sim - bs_end_mea
            
            # initialize errors
            errors = []
            
            # errors each segment
            for i_m in range(m):
                
                # initialize error of the segment
                error_seg = {'seg_type': None,
                             'parameter': None,
                             'converge': None
                }
                
                # extract segment type of the segment
                seg_type_seg_mea = seg_types_channel_mea[i_m]
                seg_type_seg_sim = seg_types_channel_sim[i_m]
                
                # register segment type info in error
                error_seg['seg_type'] = {'measurement': seg_type_seg_mea,
                                         'simulation': seg_type_seg_sim, 
                                         'seg_type_match': seg_type_seg_mea == seg_type_seg_sim
                }

                # extract parameter of the segment
                para_seg_mea = para_channel_mea[i_m]
                para_seg_sim = para_channel_sim[i_m]
                
                # normalize parameters
                para_seg_mea_norm = normalize_para(seg_type_seg_mea, para_seg_mea, scaler, i)
                para_seg_sim_norm = normalize_para(seg_type_seg_sim, para_seg_sim, scaler, i)
                para_seg_diff = abs((para_seg_mea_norm-para_seg_sim_norm)/ float(max(abs(para_seg_mea_norm), abs(para_seg_sim_norm))))
                
                error_seg['parameter'] = {'measurement': para_seg_mea_norm,
                                          'simulation':para_seg_sim_norm,
                                          'difference': para_seg_diff
                }
                
                # extract segment
                b_lf_mea = bs_end_mea[i_m] + 1
                b_rt_mea = bs_end_mea[i_m+1]
                
                b_lf_sim = bs_end_sim[i_m] + 1
                b_rt_sim = bs_end_sim[i_m+1]
                
                seg_mea = channel_mea[b_lf_mea:b_rt_mea]
                seg_sim = channel_sim[b_lf_sim:b_rt_sim]
                
                # convergence
                error_seg['error'] = {'left': seg_sim[0] - seg_mea[0],
                                      'right': seg_sim[-1] - seg_mea[-1],
                                      'convergence': None
                }
                
                error_seg['error']['convergence'] = ( (seg_sim[-2:] - seg_mea[-2:]).sum() ) / float( (seg_sim[:2] - seg_mea[:2]).sum() )
                
                errors.append(error_seg)

            interp_channel = interpret_channel(delays, errors)
            interp_sample.append(interp_channel)
            
        interp.append(interp_sample)
            
    return interp
    

#%%
def interpreter(sample, report_dir, i_sample):
    """
    interpreter: analyze/segment and compare measurement and simulation data
    """
        
    # generate folder to save results of a sample
    sample_dir = os.path.join(report_dir, 'sample_{}'.format(i_sample))
    if not os.path.isdir(sample_dir):
        os.mkdir(sample_dir)
        
    ts_plots_path = os.path.join(sample_dir, 'time_series_plots.pdf')
    interp_pred_path = os.path.join(sample_dir, 'interpretation_pred.txt')

    # extract data   
    sample_mea = sample['measurement']
    samplel_sim = sample['simulation']
    
    ts_mea = sample_mea['ts']
    ts_sim = samplel_sim['ts']
    
    bs_mea_true = sample_mea['bs_local']
    bs_sim_true = samplel_sim['bs_local']
    
    # analyse
    bs_pred, match_info = analyser(ts_mea, ts_sim)
    
    # detected local boundaries
    bs_mea_pred = bs_pred['measurement']
    bs_sim_pred = bs_pred['simulation']
    
    # match information         
    match_mea_to_sim = match_info['mea2sim']
    match_sim_to_mea = match_info['sim2mea']
    
    delays_sample, errors_sample, interp_sample = comparator(ts_mea, ts_sim, bs_mea_pred, bs_sim_pred)
    
    # save time series plot
    pp = PdfPages(ts_plots_path)
    ts_plot(pp, i_sample, ts_mea, ts_sim, bs_mea_true, bs_sim_true, bs_mea_pred, bs_sim_pred, match_mea_to_sim)
    
    # save predicted interpretation
    with open(interp_pred_path, 'w+') as f:
                
        f.write('Sample {}'.format(i_sample))
        
        N = len(interp_sample)
        
        # for univariate time series, without channel nummering
        if N == 1:
            for interp_channel in interp_sample:
                for interp_seg in interp_channel:
                    f.write(interp_seg)
                f.write('\n')
        # for multivariate time series, with channel nummering
        else:
            for i in range(N):
                f.write('\n  Channel {}'.format(i))
                interp_channel = interp_sample
                for interp_seg in interp_channel:
                    f.write(interp_seg)
                f.write('\n')
                

#%%
def interpreter_evaluate(test_data):
    """
    evaluate the performace of interpreter
    """
    
    # load data
    data_path = os.path.join('..', '..', 'data', 'pair_compare', 'test', test_data, 'samples.pkl')
    with open(data_path, 'r') as f:
        samples = pickle.load(f)
        
    # path to save report
    report_dir = os.path.join('..', '..', 'reports', 'reports_interpreter', test_data)
    if not os.path.isdir(report_dir):
        os.mkdir(report_dir)
    
    # get true interpretations
    interp_true = interpret_true(samples)
    
    # apply interpreter for each sample
    # for debug
#    for i_sample in [29]:
    for i_sample in xrange(len(samples)):
        
        # predicted interpretation of the sample
        sample = samples[i_sample]
        interpreter(sample, report_dir, i_sample)
               
        # true interpretations
        interp_true_path = os.path.join(report_dir, 'Sample_{}'.format(i_sample), 'interpretation_true.txt')
        with open(interp_true_path, 'w+') as f:
                    
            f.write('Sample {}'.format(i_sample))
            
            interp_sample_true = interp_true[i_sample]
            N = len(interp_sample_true)
            
            # for univariate time series, without channel nummering
            if N == 1:
                for interp_channel_true in interp_sample_true:
                    for interp_seg_true in interp_channel_true:
                        f.write(interp_seg_true)
                    f.write('\n')
            # for multivariate time series, with channel nummering
            else:
                for i in range(N):
                    f.write('\n  Channel {}'.format(i))
                    interp_channel_true = interp_sample_true
                    for interp_seg_true in interp_channel_true:
                        f.write(interp_seg_true)
                    f.write('\n')
         

#%% main function for the evaluation of interpreter
if __name__ == "__main__":

    # config
    test_data = 'test_mixed_1_2018_10_29_160308'
    
    # evaluate the performance of interpreter 
    interpreter_evaluate(test_data)
