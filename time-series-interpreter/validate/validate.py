# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 19:52:23 2018

@author: Yuncong
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


from ts_interpreter.analyser.analyser import analyser
from ts_interpreter.comparator.comparator import normalize_seg_horizontal, identify_seg_type, regression

#%%
def interpreter_single(ts, bs):
    """
    interpret a single univariate time series
    input: 
        ts: array with size(n, 1)
    output:
        bs: array with size (m-2, 1)
    """
    
    # length of the time series
    n = len(ts)
    
    # number of segments
    m = len(bs) + 1
    
    # boundaries with start and end time
    bs_interp = np.append(np.insert(bs, 0, 0), n-1)
    
    interps = []
    
    # analyse each segment
    for i_m in range(m):
        
        # extract segment
        b_lf = bs_interp[i_m] + 1
        b_rt = bs_interp[i_m+1]
        seg = ts[b_lf:b_rt]
        
        seg_norm = normalize_seg_horizontal(seg)
        
        seg_type = identify_seg_type(seg_norm)
        
        para_mea, para_sim = regression(seg_type, seg, seg)
        
        interp = {'seg_type': seg_type, 'para': para_mea}
        
        interps.append(interp)
        
    return interps

#%%
def reconstruct(ts, bs, interps):
    """
    calculate the reconstructed single univariate time series
    input: 
        ts: array with size (n, 1)
        bs: array with size (m-2, 1)
        interps: from interpreter_single()
    output:
        ts_recon: array with size (n, 1)
    """
    
    n = len(ts)
    m = len(interps)
    
    ts_recon = np.zeros(n)
    ts_recon[0] = ts[0]
    
    bs_recon = np.append(np.insert(bs, 0, 0), n-1)
    
    # generate each segment
    for i_m in range(m):
        
        b_lf = bs_recon[i_m]
        b_rt = bs_recon[i_m+1]
        
        seg_type = interps[i_m]['seg_type']
        para = interps[i_m]['para']
        
        # linear
        if seg_type == 0:
            
            for t in range(b_lf+1, b_rt+1):
                ts_recon[t] = ts_recon[t-1] + para
        
        # PT1
        elif seg_type == 1:
            
            for t in range(b_lf+1, b_rt+1):
                ts_recon[t] = ts_recon[b_lf] + (ts[b_rt]-ts[b_lf])*(1-np.exp(-(t-b_lf)/para))
        
        # horizontal 
        elif seg_type == 2:
            
            ts_recon[(b_lf+1):(b_rt+1)] = para
            
            if i_m == 0: 
                ts_recon[0] = para
    
    return ts_recon


#%% main function for validation with a single univariate time seris
if __name__ == "__main__":
    
    #% paths
    validate_data_path = os.path.join('..', '..', 'data', 'validate', 'Messdaten_normiert.xlsx')
    
    report_path = os.path.join('..', '..', 'reports', 'reports_validate')
    report_plot_path = os.path.join(report_path, 'validate.png')    
    report_para_path = os.path.join(report_path, 'para.xlsx')    
    
    #% import data
    df = pd.read_excel(validate_data_path)
    ts = df['z_normiert'].values.reshape(-1, 1).astype(np.float32)
    
    # segment data
    bs_pred, match_info = analyser(ts, ts)
    bs = bs_pred['measurement'][0]
    
    # interpret data
    ts = ts.flatten()
    interps = interpreter_single(ts, bs)
    ts_recon = reconstruct(ts, bs, interps)
    
    # plot
    plt.figure()
    plt.plot(ts)
    plt.plot(ts_recon, '--')
    for b in bs:
        plt.axvline(x=b, color = 'k', ls='--')
    plt.xlim(0, 100)
    plt.ylim(-0.2, 1.2)
    plt.legend(('original', 'reconstructed'), loc=9, bbox_to_anchor=(0.5, 1.15), ncol=2)
    plt.xlabel('Time Step')
    plt.ylabel('Temperature (normalized)')
    plt.grid(True)
    plt.savefig(report_plot_path)    